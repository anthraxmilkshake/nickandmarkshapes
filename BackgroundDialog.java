// Background Dialog

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.beans.*;

public class BackgroundDialog extends JDialog implements ActionListener, PropertyChangeListener 
{
    private JPanel myPanel = null;
    private JButton OKButton = null, cancelButton = null;
    private JPanel buttonPanel = null;    
    private Color currentColor;
    private JColorChooser chooser = null;
    private boolean answer = false;
    public Color getColor() { return currentColor; }
    public boolean getAnswer() { return answer; }
    
    public BackgroundDialog(JFrame frame, boolean modal, int x, int y)
    {
	super(frame, modal);
	myPanel = new JPanel();
	getContentPane().add(myPanel);
	myPanel.setLayout (new FlowLayout());
	addTextAndButtons ();
	setTitle ("Change Background Color");
	setLocation (x, y);
	setSize (650,380);
	setVisible(true);
    }
	    
    private void addTextAndButtons ()
    {
	chooser = new JColorChooser(Color.black);
        chooser.addPropertyChangeListener(this);
        myPanel.add(chooser);
	
	buttonPanel = new JPanel();
	OKButton = new JButton("OK");
	OKButton.addActionListener(this);
	buttonPanel.add(OKButton); 
	cancelButton = new JButton("Cancel");
	cancelButton.addActionListener(this);
	buttonPanel.add(cancelButton); 
	myPanel.add(buttonPanel); 
    }

    public void propertyChange(PropertyChangeEvent e)
    {
        currentColor = chooser.getColor();
    }
    
    public void actionPerformed(ActionEvent e) 
    {
	if (OKButton == e.getSource())
	    {
		answer = true;
		setVisible(false);
		getContentPane().remove(myPanel);
	    }
	else if(cancelButton == e.getSource()) 
	    {
		answer = false;
		setVisible(false);
	    }
    }
    
} 

