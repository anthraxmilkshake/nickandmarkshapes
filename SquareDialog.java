// file: SquareDialog.java
// CS 360 - Fall 2006 - Watts
// Project 1
// September 2006
// Written by Dr. Watts
// http://www.cs.sonoma.edu/~tiawatts 
/*
Dialog box for selecting a shape and its color and providing 
a name for the shape
*/

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.beans.*;

public class SquareDialog extends JDialog implements ActionListener, PropertyChangeListener 
{
    private JPanel myPanel = null;
    private JButton OKButton = null, cancelButton = null;
    private JTextField radiusText;
    private JTextField angleText;
    private ButtonGroup shapeGroup = null;
    private JPanel  buttonPanel = null;
    private JColorChooser chooser = null;
    private Color currentColor = Color.red;
    private int oldRadius = 0;
    private int radius = 0;
    private double angle = 0.0;
    private double oldAngle = 0.0;
    private boolean answer = false;
    public Color getColor() { return currentColor; }
    public int getRadius() { return radius; }
    public double getAngle() { return angle; }
    public boolean getAnswer() { return answer; }
    
    public SquareDialog(JFrame frame, boolean modal, int x, int y, int R, double A)
    {
	super(frame, modal);
	oldRadius = R;
	oldAngle = A;
	myPanel = new JPanel();
	getContentPane().add(myPanel);
	myPanel.setLayout (new FlowLayout());
	addTextAndButtons ();
	setTitle ("Modify Square Dialog");
	setLocation (x, y);
	setSize (650,420);
	setVisible(true);
    }
	
    private void addTextAndButtons ()
    {
	chooser = new JColorChooser(Color.red);
	chooser.addPropertyChangeListener(this);
	myPanel.add(chooser);
	
	myPanel.add(new JLabel("Enter the side length:"));
	radiusText = new JTextField(((Integer) oldRadius).toString(), 30);
	radiusText.addActionListener(this);
	myPanel.add (radiusText);
	
	myPanel.add(new JLabel("Enter the angle amount:"));
	angleText = new JTextField(((Double) oldAngle).toString(), 30);
	angleText.addActionListener(this);
	myPanel.add(angleText);
	
	buttonPanel = new JPanel();
	OKButton = new JButton("OK");
	OKButton.addActionListener(this);
	buttonPanel.add(OKButton); 
	cancelButton = new JButton("Cancel");
	cancelButton.addActionListener(this);
	buttonPanel.add(cancelButton); 
	myPanel.add(buttonPanel); 
    }
    
    public void propertyChange(PropertyChangeEvent e)
    {
	currentColor = chooser.getColor();
    }

    public void actionPerformed(ActionEvent e) 
    {
	if(OKButton == e.getSource()) 
	    {
		answer = true;
		setVisible(false);
		getContentPane().remove(myPanel);
		try
		    {
			radius = Integer.parseInt (radiusText.getText());
			angle = Double.parseDouble(angleText.getText());
		    }
		catch (NumberFormatException ex)
		    {
			radius = oldRadius;
			angle = oldAngle;
		    }
	    }
	else if(cancelButton == e.getSource()) 
	    {
		answer = false;
		setVisible(false);
	    }
    }
} 

