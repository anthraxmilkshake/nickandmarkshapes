// File: cPolygon.java
// Author: Dr. Watts
// Contents: This file contains the description and implementation
// of a class called cPolygon. 

import java.awt.*;

import static java.lang.Math.*;
import java.awt.geom.*;
import javax.swing.*;

public class cPolygon extends Shape
{
    protected int [] vertexX = new int [0];
    protected int [] vertexY = new int [0];
    protected Polygon polygon = new Polygon ();
    protected double angle = 0.0;
    protected int sides = 0;

    cPolygon ()
    {
    }

    public void resize(double distance)
    {
	side += distance;
	calculatePoints();
	rotatePoly();
    }
    public void rotate(double distance)
    {
	angle += distance;
	calculatePoints();
	rotatePoly();
    }

    public void modifyShape (JFrame frame, int x, int y)
    {
        cPolygonDialog cpolygondialog = new cPolygonDialog (frame, true, x, y, 
							    side, angle, sides); 
        side = cpolygondialog.getRadius ();
        sides = cpolygondialog.getSides ();
        color = cpolygondialog.getColor ();
        angle = cpolygondialog.getAngle();
	
	calculatePoints();
	rotatePoly();
    }

    public String getName ()
    {
	return "cPolygon";
    }
    public void setCenterX (int X)
    {
	centerX = X;
	calculatePoints ();
    }
    public void setCenterY (int Y)
    {
	centerY = Y;
	calculatePoints ();
    }
    public void paintComponent (Graphics2D g2)
    {
    	g2.setStroke(new BasicStroke(strokeSize));
        g2.setPaint(strokeColor);
        g2.draw (polygon);
        g2.setPaint (color);
	g2.fill (polygon);
	
	g2.setPaint (Color.BLACK);
	g2.fillOval (centerX-1, centerY-1, 2, 2);  
    }

    protected void calculatePoints()
    {	
	polygon.reset();
	polygon.invalidate();
	double theta = 2 * Math.PI / sides;
	for (int i = 0; i < sides; i++)
	    {
		int a = (int)(Math.cos(theta * i) * side);
		int b = (int)(Math.sin(theta * i) * side);
	
		polygon.addPoint(a, b);
	    }
	polygon.translate(centerX, centerY);
	setVertices();
    }

    protected void setVertices ()
    {
	vertexX = new int[sides];
	vertexY = new int[sides];
        PathIterator i = polygon.getPathIterator(new AffineTransform());
        for (int s = 0; s < sides; s++) {
            	    
            double[] xy = new double[2];
            i.currentSegment(xy);
            vertexX[s] = (int)xy[0];
	    vertexY[s] = (int)xy[1];
	    
	    i.next();
            
        }
    }
    public void rotatePoly()
    {
	AffineTransform transformer = new AffineTransform();
	transformer.rotate(
			   Math.toRadians(angle), (double)centerX, (double)centerY);
	
	
	Polygon r2 = new Polygon();
        PathIterator i = polygon.getPathIterator(transformer);
        for (int s = 0; s < sides; s++) {
            	    
            double[] xy = new double[2];
            i.currentSegment(xy);
            r2.addPoint((int) xy[0], (int) xy[1]);
	    
	    i.next();
            
        }
	
	polygon = r2;
	setVertices();
	
    }

    public boolean isIn (int X, int Y)
    {
	return polygon.contains (X, Y);
    }

    public void move (int deltaX, int deltaY)
    {
	centerX += deltaX;
	centerY += deltaY;

	polygon.translate(deltaX, deltaY);
	setVertices();	

    }
    public void fromString (String str)
    {
        String [] parts = str.split (" ");
        try
            {
                centerX = Integer.parseInt(parts[0]);
                centerY = Integer.parseInt(parts[1]);
                side = Integer.parseInt(parts[2]);
		sides = Integer.parseInt(parts[3]);
                color = new Color(Integer.parseInt(parts[4]));
                angle = Double.parseDouble (parts[5]);

		calculatePoints();
                rotatePoly();            
                
            }
        catch (NumberFormatException e)
            {
                System.out.println ("File input error - invalid integer");
            }
    }
    
    public String toString ()
    {
        String string = new String ();
        string += centerX + " ";
        string += centerY + " ";
        string += side + " ";
        string += sides + " ";
        string += color.getRGB() + " ";
        string += angle + " ";
        return string;
    }

}
