// File: Scalene.java
// Author: Dr. Watts
// Contents: This file contains the description and implementation
// of a class called Scalene. 

import static java.lang.Math.*;
import java.awt.*;
import javax.swing.*;
import java.awt.geom.*;

public final class Scalene extends Triangle
{

    private int side2;
    private int side3;
    
    public Scalene ()
    {
	side = 10;
	side2 = 10;
	side3 = 10;
    }

    public void updateSides(double [] xpoints, double [] ypoints)
    {
    side = (int)distance(xpoints[0], xpoints[1], ypoints[0], ypoints[1]);
	side2 = (int)distance(xpoints[1], xpoints[2], ypoints[1], ypoints[2]);
	side3 = (int)distance(xpoints[2], xpoints[0], ypoints[2], ypoints[0]);
	calculateTriangle();
    }
    public void resize(double distance)
    {
    	double newdistance = distance;
    	//speed limiting the growth
    	if (newdistance > 5)
    		newdistance = 5;
    	if (newdistance <-5)
    		newdistance = -5;
    	//resizing
    	side += (int)((double)side * (newdistance/100));
    	side2 += (int)((double)side2 * (newdistance/100));
    	side3 += (int)((double)side3 * (newdistance/100));
    	calculateTriangle();
    	rotate();
    }

	
    public void calculateTriangle()
    {

	double cosAngle = (side * side + side2 * side2 - side3 * side3) / (2.0 * side * side2);
	double angle = acos (cosAngle);
	int height = (int) (sin(angle) * side2);
	int offX = (int) (cos(angle) * side2);
	vertexX[0] = vertexY[0] = 0;
	vertexX[1] = offX; vertexY[1] = -height;
	vertexX[2] = side; vertexY[2] = 0;
	int inX = (vertexX[0]* side3 + vertexX[1] * side + vertexX[2] * side2) / (int) perimeter();
	int inY = (vertexY[0]* side3 + vertexY[1] * side + vertexY[2] * side2) / (int) perimeter();
	for (int i = 0; i < 3; i++)
	    {
		vertexX[i] += (centerX - inX);
		vertexY[i] += (centerY - inY);
	    }

	polygon = new Polygon (vertexX, vertexY, 3);
	

    }
    public void modifyShape (JFrame frame, int x, int y)
    {
	ScaleneDialog scalenedialog = new ScaleneDialog (frame, true, x, y, side, side2, side3, angle); 
	side = scalenedialog.getSide ();
	side2 = scalenedialog.getSide2 ();
	side3 = scalenedialog.getSide3 ();
	color = scalenedialog.getColor ();
	angle = scalenedialog.getAngle();
	calculateTriangle();
	rotate();
    }
	
    public Scalene (Scalene E)
    {
	side = E.side;
	side2 = E.side2;
	side3 = E.side3;
	centerX = E.centerX;
	centerY = E.centerY;
	color = E.color;
	calculateTriangle();
	rotate();
    }
    
    public Scalene(int S, int S2, int S3, int X, int Y, Color C)
    {
	side = S;
	side2 = S2;
	side3 = S3;
	centerX = X;
	centerY = Y;
	color = C;
	calculateTriangle();
	rotate();
	
    }

    public void setSide (double S)
    {
	side =(int) S;
	calculateTriangle();
    }
	
    public double getSide ()
    {
	return side;
    }
    public void setSide2 (double S)
    {
	side2 =(int) S;
	calculateTriangle();
    }
	
    public double getSide2 ()
    {
	return side2;
    }
    public void setSide3 (double S)
    {
	side3 =(int) S;
	calculateTriangle();
    }
	
    public double getSide3 ()
    {
	return side3;
    }
	
    public double perimeter ()
    {
	return side + side2 + side3;
    }
	
    public double area ()
    {
	return sqrt(3) * side * side / 4;
    }
	
    public String getName ()
    {
	return "Scalene";
    }
    public void fromString (String str)
    {
	String [] parts = str.split (" ");
	try
	    {
		centerX = Integer.parseInt(parts[0]);
		centerY = Integer.parseInt(parts[1]);
		side = Integer.parseInt(parts[2]);
		side2 = Integer.parseInt(parts[3]);
		side3 = Integer.parseInt(parts[4]);
		color = new Color(Integer.parseInt(parts[5]));
		angle = Double.parseDouble (parts[6]);
		calculateTriangle();
		rotate();			

	    }
	catch (NumberFormatException e)
	    {
		System.out.println ("File input error - invalid integer");
	    }
    }

    public String toString ()
    {
	String string = new String ();
	string += centerX + " ";
	string += centerY + " ";
	string += side + " ";
	string += side2 + " ";
	string += side3 + " ";
	string += color.getRGB() + " ";
	string += angle + " ";
	return string;
    }

}
