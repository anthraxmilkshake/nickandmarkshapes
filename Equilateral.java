// File: Equilateral.java
// Author: Dr. Watts
// Contents: This file contains the description and implementation
// of a class called Equilateral. 

import static java.lang.Math.*;
import java.awt.*;
import javax.swing.*;
public final class Equilateral extends Triangle
{
    
    public Equilateral ()
    {
    }
    
    public Equilateral (Equilateral E)
    {
	side = E.side;
	centerX = E.centerX;
	centerY = E.centerY;
	color = E.color;
	calculateTriangle();
	rotate();
    }
    
    public Equilateral (double S)
    {
	side = (int)S;
	calculateTriangle();
    }
    public void modifyShape (JFrame frame, int x, int y)
    {
	EquilateralDialog equilateraldialog = new EquilateralDialog (frame, true, x, y, 
								     side, angle); 
	side = equilateraldialog.getRadius ();
	color = equilateraldialog.getColor ();
	angle = equilateraldialog.getAngle();
	calculateTriangle ();
	rotate();
    }
    
    public Equilateral(int S, int X, int Y, Color C)
    {
	side = S;
	centerX = X;
	centerY = Y;
	color = C;
	calculateTriangle();
	rotate();
	
    }
    
    public void setSide (double S)
    {
	side =(int) S;
	calculateTriangle();
    }
    
    public double getSide ()
    {
	return side;
    }
    
    public double perimeter ()
    {
	return 3 * side;
    }
    
    public double area ()
    {
	return sqrt(3) * side * side / 4;
    }
    
    public String getName ()
    {
		return "Equilateral";
    }
    
    public void fromString (String str)
    {
	String [] parts = str.split (" ");
	try
	    {
		centerX = Integer.parseInt(parts[0]);
		centerY = Integer.parseInt(parts[1]);
		side = Integer.parseInt(parts[2]);
		
		color = new Color(Integer.parseInt(parts[3]));
		angle = Double.parseDouble (parts[4]);
		calculateTriangle();	
		rotate();		
		
	    }
	catch (NumberFormatException e)
	    {
		System.out.println ("File input error - invalid integer");
	    }
    }
    
    public String toString ()
    {
	String string = new String ();
	string += centerX + " ";
	string += centerY + " ";
	string += side + " ";
	
	string += color.getRGB() + " ";
	string += angle + " ";
	return string;
    }
    
}
