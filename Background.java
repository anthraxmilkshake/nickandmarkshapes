// File: Background.java
// Author: Dr. Watts
// Contents: This file contains the implementation of a small
// GUI application that uses the Shape class hierarchy.


import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.imageio.*;
import javax.swing.*;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.Timer;
import java.util.concurrent.TimeUnit;
import java.awt.Graphics;
import static java.lang.Math.*;


public class Background extends JPanel implements ActionListener, MouseMotionListener, 
						  MouseListener, KeyListener
{
    private JButton saveButton;
    private JButton saveImageButton;
    private JButton bkndColorButton;
    private JButton deleteAllButton;
    private JButton addShapeButton;
    private JButton deleteShapeButton;
    private JButton loadShapesButton;
    private JButton kaboomButton;
    private JButton backgroundImageButton;
    private JFrame outside;
    private JPanel buttonPanel;
    private Timer timer;
    private TimerTask timerTask;
    private int counter = 0;
    private boolean createMode = false;
    private boolean deleteMode = false;
    private boolean resizeMode = false;
    private boolean rotateMode = false;
    private boolean multiSelectMode = false;
    private boolean inFrame = true;
    private int currentX, currentY;
    private ArrayList <Shape> S = new ArrayList <Shape> ();
    private ArrayList <Shape> SelectedList = new ArrayList <Shape> ();
    private Shape selected = null;
    private File backimage;
    private String backimagename;
    private boolean backgroundimage = false;
    
    public Background ()
    {
	repaint();
    }
    public Background (JFrame frame, String [] files)
    {
	outside = frame;
	setLayout(new BorderLayout());
	JToolBar buttonPanel = new JToolBar (JToolBar.VERTICAL);
	
	add(buttonPanel, BorderLayout.WEST);
	buttonPanel.setLayout(new GridLayout(9,1));

	saveButton = new JButton ("Save Shapes");
	buttonPanel.add (saveButton) ;
	saveButton.addActionListener (this);
	
	saveImageButton = new JButton ("Save Image");
	buttonPanel.add (saveImageButton) ;
	saveImageButton.addActionListener (this);
	
	loadShapesButton = new JButton ("Load Shapes");
	buttonPanel.add (loadShapesButton) ;
	loadShapesButton.addActionListener (this);
	
	bkndColorButton = new JButton ("Change Background Color");
	buttonPanel.add (bkndColorButton);
	bkndColorButton.addActionListener (this);

	addShapeButton = new JButton ("Add Shape");
	buttonPanel.add (addShapeButton);
	addShapeButton.addActionListener (this);

	deleteShapeButton = new JButton ("Delete A Shape");
	buttonPanel.add (deleteShapeButton);
	deleteShapeButton.addActionListener (this);

	deleteAllButton = new JButton ("Delete All Shapes");
	buttonPanel.add (deleteAllButton);
	deleteAllButton.addActionListener (this);
		
	kaboomButton = new JButton ("KABOOM!!!");
	buttonPanel.add (kaboomButton);
	kaboomButton.addActionListener (this);
	
	backgroundImageButton = new JButton ("Background Image!");
	buttonPanel.add (backgroundImageButton);
	backgroundImageButton.addActionListener (this);
	
	
	

	
	
	
	
	
	setBackground (Color.BLACK);
	addMouseMotionListener(this);
	addMouseListener(this);
	setFocusable(true);
	requestFocus();
	addKeyListener(this); 
	

	ShapeIO shapeIO = new ShapeIO ();
	for (int i = 0; i < files.length; i++)
	    {
		shapeIO.readShapes (files[i], S);
	    }
	repaint();
    }
    
    public void mouseDragged(MouseEvent e)
    {
    for (int i = 0; i< SelectedList.size(); i++)
    {
    	selected = SelectedList.get(i);
	// Move
	if (inFrame && selected != null && resizeMode == false && rotateMode == false) 
	    {
		selected.move (e.getX() - currentX, e.getY() - currentY);
		repaint();

	    }
	// Resize
	if (inFrame && selected != null && resizeMode == true)
	    {	double oldDistance = selected.resizeDistance;
		selected.resizeDistance = sqrt( ((e.getX() - selected.centerX) * (e.getX()- selected.centerX)) + ((e.getY() - selected.centerY) * (e.getY() - selected.centerY)) );		
		selected.resize ((selected.resizeDistance - oldDistance));
		repaint();
	    }
	
	// Rotate
	if (inFrame && selected != null && rotateMode == true)
	    {
		double oldDistance = selected.resizeDistance;
		selected.resizeDistance = sqrt( ((e.getX() - selected.centerX) * (e.getX()- selected.centerX)) + ((e.getY() - selected.centerY) * (e.getY() - selected.centerY)) );		
		selected.rotate ((selected.resizeDistance - oldDistance));
		repaint();
	    }
    }
    if (inFrame && selected != null && resizeMode == false && rotateMode == false) 
    {
		currentX = e.getX();
		currentY = e.getY();
    }
    }
    public void mouseMoved(MouseEvent e) {}
    public void paintComponent (Graphics g)
    {
	super.paintComponent (g);
	Graphics2D g2 = (Graphics2D) g;
	g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
			    RenderingHints.VALUE_ANTIALIAS_ON);
	
	//Background Image Handler
	
	if (backgroundimage == true)
	{
		try {
	
		backimage = new File(backimagename);
		BufferedImage img = ImageIO.read(backimage);
		ImageObserver observer = null;
		g2.drawImage(img, 0, 0, getWidth(), getHeight(),img.getMinX(),img.getMinY(),img.getWidth(),img.getHeight(),  observer);
		
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
    }
	
	
	
	
//shape handler
	for (int i = 0; i < S.size(); i++)
	    {
		S.get(i).paintComponent (g2);
	    }
    
    }
    public void actionPerformed (ActionEvent e)
    {
 
	if (e.getSource() == saveButton)
	    {
		FileDialog SD = new FileDialog (outside, true, 100, 100, "Save Shapes to File");
		ShapeIO shapeIO = new ShapeIO ();
		String filename = SD.getFilename();
		shapeIO.writeShapes (filename, S);
	    }
	if (e.getSource() == saveImageButton)
    {
	FileDialog SD = new FileDialog (outside, true, 100, 100, "Save Shapes to Image File");

	String filename = SD.getFilename();
	saveImage(filename);
    }
	if (e.getSource() == loadShapesButton)
	    {
		FileDialog LD = new FileDialog (outside, true, 100, 100, 
						"Load Shapes from File");
		ShapeIO shapeIO = new ShapeIO ();
		String files = LD.getFilename();
		shapeIO.readShapes (files, S);
		repaint();
	    }
	if (e.getSource() == backgroundImageButton)
    {
	FileDialog LD = new FileDialog (outside, true, 100, 100, 
					"Select Background Image");
	backimagename = LD.getFilename();
	backgroundimage = true;
	
	repaint();
    }

	if (e.getSource() == bkndColorButton)
	    {
		BackgroundDialog BD = new BackgroundDialog (outside, true, 100, 100);
		setBackground (BD.getColor());
		backgroundimage = false;
		repaint();
	    }
	if (e.getSource() == addShapeButton)
	    {
		createMode = true;
	    }
		
	if (e.getSource() == deleteAllButton)
	    {
		S.clear();
		repaint();
	    }		
	
	if (e.getSource() == deleteShapeButton)
	    {
		deleteMode = true;
	    }
	
	if (e.getSource() == kaboomButton)
    {
		
			
		timerTask = new TimerTask()
		{
			int halfwidth = getWidth()/2;
			int halfheight = getHeight()/2;
	    	int speed = 60;
			@Override
	    	public void run()
	    	{
	    		counter++;
	    		
	    		for (int j = 0; j<S.size(); j++)
	    		{
	    			if(S.get(j).centerX <= halfwidth && S.get(j).centerY <= halfheight)
	    				S.get(j).move(-speed, -speed);
	    			else if(S.get(j).centerX > halfwidth && S.get(j).centerY <= halfheight)
	    				S.get(j).move(speed, -speed);
	    			else if(S.get(j).centerX <= halfwidth && S.get(j).centerY > halfheight)
	    				S.get(j).move(-speed, speed);
	    			else
	    				S.get(j).move(speed, speed);
	    			repaint();
	    		}
	    		if (counter == 100)
	    		{
	    			timer.cancel();
	    		S.clear();
	    		repaint();
	    		counter =0;
	    		}
			}
	    	
		};
								

			timer = new Timer( "A Timer");
			timer.scheduleAtFixedRate(timerTask,0,41);
		
		
		
	
    }

}

    private void saveImage(String filename) {
    	
		BufferedImage bi = new BufferedImage(this.getSize().width, this.getSize().height, BufferedImage.TYPE_BYTE_INDEXED); 
		Graphics g = bi.createGraphics();
		this.paint(g);  //this == JComponent
		g.dispose();
		filename = filename+".jpg";

		try{ImageIO.write(bi,"jpg", new File(filename));}catch (Exception e) {}
		
	}
	public void mousePressed (MouseEvent e)
    {
	inFrame = true;
	requestFocusInWindow();
    currentX = e.getX();
	currentY = e.getY();
	selected = null;

	// Moving
	if ((e.getButton() == e.BUTTON1) && createMode == false && deleteMode == false && resizeMode==false && rotateMode == false && multiSelectMode == false) 
	    {
		for (int i = S.size()-1; selected == null && i >= 0; i--)
		    if (S.get(i).isIn(currentX, currentY) && !SelectedList.contains(S.get(i)))
			{
		    	SelectedList.add(S.get(i));
		    	S.get(i).strokeColor=Color.white;
		    	S.get(i).strokeSize=10;
		    	break;
		    	
			}
	    }
	    
	// Deleting
	 if ((e.getButton() == e.BUTTON1) && deleteMode == true) 
	{
	    deleteMode = false;
		for (int i = S.size()-1; selected == null && i >= 0; i--)
		    if (S.get(i).isIn(currentX, currentY) && !SelectedList.contains(S.get(i)))
			{
			    S.remove(i);
			    repaint();
			    break;
			}
	    }
	// MultiSelecting
	else if ((e.getButton() == e.BUTTON1) && multiSelectMode == true) 
	{

		for (int i = S.size()-1; i >= 0; i--)
		    if (S.get(i).isIn(currentX, currentY) && !SelectedList.contains(S.get(i)))
			{
		    	SelectedList.add(S.get(i));
		    	S.get(i).strokeColor=Color.white;
		    	S.get(i).strokeSize=10;
			}
	    }
	// Creating
	else if (((e.getButton() == e.BUTTON1) && createMode == true) 
		 || e.getButton() ==e.BUTTON3)
	    {
		createMode = false;
		for (int i = S.size()-1; selected == null && i >= 0; i--)
		    if (S.get(i).isIn(currentX, currentY) && !SelectedList.contains(S.get(i)))
			{
		    	//SelectedList.add(S.get(i));
		    	selected = S.get(i);
			  
			}
		if (selected != null)
		    selected.modifyShape (outside, e.getX(), e.getY());
		else
		    {	
			ShapeDialog shapedialog = new ShapeDialog(outside, true, 
								  e.getX(), e.getY());
			if (shapedialog.getAnswer() == true)
			    {
				Shape newshape = shapedialog.getMyShape();
				S.add(newshape);
				newshape.setCenterX(e.getX());
				newshape.setCenterY(e.getY());
				newshape.modifyShape (outside, e.getX(), e.getY());
			    }
		    }
		repaint();
	    }
	// Resizing
	else if ((e.getButton() == e.BUTTON1) && (resizeMode == true))
	    {
		
		for (int i = S.size()-1; selected == null && i >= 0; i--)
		    if (S.get(i).isIn(currentX, currentY) && !SelectedList.contains(S.get(i)))
			{
		    	SelectedList.add(S.get(i));
		    	S.get(i).strokeColor=Color.white;
		    	S.get(i).strokeSize=10;
		    	break;
			}
		selected.resizeDistance = sqrt ( ((e.getX() - selected.centerX) * (e.getX()- selected.centerX)) + ((e.getY() - selected.centerY) * (e.getY() - selected.centerY)));
	    }
	// Rotating
	else if ((e.getButton() == e.BUTTON1) && (rotateMode == true))
	    {
		for (int i = S.size()-1; selected == null && i >= 0; i--)
		    if (S.get(i).isIn(currentX, currentY) && !SelectedList.contains(S.get(i)))
			{
		    	SelectedList.add(S.get(i));
		    	S.get(i).strokeColor=Color.white;
		    	S.get(i).strokeSize=10;
		    	break;
			}
		selected.resizeDistance = sqrt ( ((e.getX() - selected.centerX) * (e.getX()- selected.centerX)) + ((e.getY() - selected.centerY) * (e.getY() - selected.centerY)));		
}
	 repaint();
	
 
    }
    public void mouseReleased (MouseEvent e) {
    	
    	if (multiSelectMode == false)
    	{
    		
    		for (int i=0; i< SelectedList.size(); i++)
    			{SelectedList.get(i).strokeColor = SelectedList.get(i).color;
    		SelectedList.get(i).strokeSize=0;
    			}
    		SelectedList.clear();
    		selected = null;
    		repaint();
    	}
    }
    public void mouseEntered (MouseEvent e) 
    {
	inFrame = true;
    }
    public void mouseExited (MouseEvent e)
    {
	inFrame = false;
	selected = null;
    }
    public void mouseClicked (MouseEvent e) {}

    public void keyPressed(KeyEvent e)
    {
	if (e.getKeyCode() == KeyEvent.VK_DELETE)
	    deleteMode = true;
	if (e.getKeyCode() == KeyEvent.VK_SHIFT)
	    resizeMode = true;
	if (e.getKeyCode() == KeyEvent.VK_R)
	    rotateMode = true;
	if (e.getKeyCode() == KeyEvent.VK_CONTROL)
	    multiSelectMode = true;
    }

    public void keyReleased(KeyEvent e)
    {
	if (e.getKeyCode() == KeyEvent.VK_SHIFT)
	    resizeMode = false;
	if (e.getKeyCode() == KeyEvent.VK_R)
	    rotateMode = false;
	if (e.getKeyCode() == KeyEvent.VK_CONTROL)
	    multiSelectMode = false;
    }
    
    public void keyTyped(KeyEvent e)
    {}

		
	}

