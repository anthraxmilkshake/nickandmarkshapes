// File: Rectangle.java
// Author: Dr. Watts
// Contents: This file contains the description and implementation
// of a class called Rectangle. 
import java.text.NumberFormat;
import static java.lang.Math.*;
import java.awt.*;
import javax.swing.*;
import java.awt.geom.*;


public final class Rectangle extends Quadrilateral
{

    private int side2;

    public Rectangle ()
    {
	
    }
    public void resize (double distance)
    {
    	double newdistance = distance;
    	//speed limiting the growth
    	if (newdistance > 5)
    		newdistance = 5;
    	if (newdistance <-5)
    		newdistance = -5;
    	//resizing
    	side += (side * (newdistance/100));
    	side2 += (side2 *(newdistance/100));
    	updatePoly();
    	rotateQuad();	

    }
    public void updateSides ()
    {
	int [] vertexX = new int[4];
	int [] vertexY = new int[4];
        PathIterator i = polyrect.getPathIterator(new AffineTransform());
        for (int s = 0; s < 4; s++) {

            double[] xy = new double[2];
            i.currentSegment(xy);
            vertexX[s] = (int)xy[0];
            vertexY[s] = (int)xy[1];

            i.next();

        }
	side2 = (int)sqrt(((vertexX[0]-vertexX[1])*(vertexX[0]-vertexX[1])) + (((vertexY[0]-vertexY[1])*(vertexY[0]-vertexY[1]))));
	side = (int)sqrt(((vertexX[2]-vertexX[3])*(vertexX[2]-vertexX[3])) + (((vertexY[2]-vertexY[3])*(vertexY[2]-vertexY[3]))));

    }

    public Rectangle (Rectangle R)
    {
	side = R.side;
	side2 = R.side2;
	centerX = R.centerX;
	centerY = R.centerY;
	color = R.color;
	angle = R.angle;
	updatePoly();
	rotateQuad();

	
    }
    public void updatePoly()
    {
	polyrect.reset();
	polyrect.invalidate();
	polyrect.addPoint(centerX-side/2, centerY-side2/2);
	polyrect.addPoint(centerX-side/2, centerY+side2/2);
	polyrect.addPoint(centerX+side/2, centerY+side2/2);		
	polyrect.addPoint(centerX+side/2, centerY-side2/2);
		
    }
    public Rectangle (double S1, double S2)
    {
	side = (int)S1;
	side2 = (int)S2;

	updatePoly();
	rotateQuad();

    }
    public Rectangle (int S, int S2, int X, int Y, Color C, double R)
    {
	side = S;
	side2 = S2;
	centerX = X;
	centerY = Y;
	color = C;
	angle = R;
	updatePoly();
	rotateQuad();

    }
    public void modifyShape (JFrame frame, int x, int y)
    {
	RectangleDialog rectangledialog = new RectangleDialog (frame, true, x, y, side, side2, angle); 
	side = rectangledialog.getSide ();
	side2 = rectangledialog.getSide2 ();
	color = rectangledialog.getColor ();
	angle = rectangledialog.getAngle();		
	updatePoly();
	rotateQuad();		

    }
    
    public void setSide1 (double S1)
    {
	side = (int)S1;
	updatePoly();
	rotateQuad();

    }
	
    public double getSide1 ()
    {
	return side;
    }
    public void setSide2 (double S2)
    {
	side2 = (int)S2;

    }
	
    public double getSide2 ()
    {
	return side2;
    }
       
	
    public double perimeter ()
    {
	return side * side2 * 2;
    }
	
    public double area ()
    {
	return side * side2;
    }
	
    public String getName ()
    {
	return "Rectangle";
    }
	
    public void move (int deltaX, int deltaY)
    {
	centerX += deltaX;
	centerY += deltaY;
	polyrect.translate(deltaX,deltaY);

		
	
    }




    public void fromString (String str)
    {
	String [] parts = str.split (" ");
	try
	    {
		centerX = Integer.parseInt(parts[0]);
		centerY = Integer.parseInt(parts[1]);
		side = Integer.parseInt(parts[2]);
		side2 = Integer.parseInt(parts[3]);
		color = new Color(Integer.parseInt(parts[4]));
		angle = Double.parseDouble (parts[5]);

		updatePoly();
		rotateQuad();
			
	    }
	catch (NumberFormatException e)
	    {
		System.out.println ("File input error - invalid integer");;
	    }
    }
    public String toString ()
    {
	String string = new String ();
	string += centerX + " ";
	string += centerY + " ";
	string += side + " ";
	string += side2 + " ";
	string += color.getRGB() + " ";
	string += angle + " ";
	return string;
    }
}	
	
