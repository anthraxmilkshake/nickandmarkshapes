// file: ScaleneDialog.java
// CS 360 - Fall 2006 - Watts
// Project 1
// September 2006
// Written by Dr. Watts
// http://www.cs.sonoma.edu/~tiawatts 
/*
Dialog box for selecting a shape and its color and providing 
a name for the shape
*/

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.beans.*;

public class ScaleneDialog extends JDialog implements ActionListener, PropertyChangeListener 
{
    private JPanel myPanel = null;
    private JButton OKButton = null, cancelButton = null;
    private JTextField sideText;
    private JTextField side2Text;
    private JTextField side3Text;
    private JColorChooser chooser = null;
    private JTextField angleText;
    private double angle = 0.0;
    private double oldAngle = 0.0;
    private ButtonGroup shapeGroup= null;
    private JPanel buttonPanel = null;    
    private Color currentColor = Color.red;
    private int oldSide = 0;
    private int side = 0;
    private int oldSide2 = 0;
    private int side2 = 0;
    private int oldSide3 = 0;
    private int side3 = 0;
    private boolean answer = false;
    public Color getColor() { return currentColor; }
    public int getSide() { return side; }
    public int getSide2() { return side2; }
    public int getSide3() { return side3; }
    public boolean getAnswer() { return answer; }
    public double getAngle() { return angle; }	
    
    public ScaleneDialog(JFrame frame, boolean modal, int x, int y, int S, int S2, 
			 int S3, double A)
    {
	super(frame, modal);
	oldSide= S;
	oldSide2 = S2;
	oldSide3 = S3;
	oldAngle = A;
	myPanel = new JPanel();
	getContentPane().add(myPanel);
	myPanel.setLayout (new FlowLayout());
	addTextAndButtons ();
	setTitle ("Modify Scalene Dialog");
	setLocation (x, y);
	setSize (650,475);
	setVisible(true);
    }

    private void addTextAndButtons ()
    {
	chooser = new JColorChooser(Color.red);
	chooser.addPropertyChangeListener(this);
	myPanel.add(chooser);
	
	myPanel.add(new JLabel("Enter the first side:"));
	sideText = new JTextField(((Integer) oldSide).toString(), 30);
	sideText.addActionListener(this);
	myPanel.add (sideText);
	
	myPanel.add(new JLabel("Enter the second side:"));
	side2Text = new JTextField(((Integer) oldSide2).toString(), 30);
	side2Text.addActionListener(this);
	myPanel.add (side2Text);
	
	myPanel.add(new JLabel("Enter the third side:"));
	side3Text = new JTextField(((Integer) oldSide3).toString(), 30);
	side3Text.addActionListener(this);
	myPanel.add (side3Text);
	
	myPanel.add(new JLabel("Enter the angle amount:"));
	angleText = new JTextField(((Double) oldAngle).toString(), 30);
	angleText.addActionListener(this);
	myPanel.add(angleText);
	
	buttonPanel = new JPanel();
	OKButton = new JButton("OK");
	OKButton.addActionListener(this);
	buttonPanel.add(OKButton); 
	cancelButton = new JButton("Cancel");
	cancelButton.addActionListener(this);
	buttonPanel.add(cancelButton); 
	myPanel.add(buttonPanel); 
    }
    
    public void propertyChange(PropertyChangeEvent e)
    {
        currentColor = chooser.getColor();
    }
    
    public void actionPerformed(ActionEvent e) 
    {
	if(OKButton == e.getSource()) 
	    {
		answer = true;
		setVisible(false);
		getContentPane().remove(myPanel);
		try
		    {
			side = Integer.parseInt (sideText.getText());
			side2 = Integer.parseInt (side2Text.getText());
			side3 = Integer.parseInt (side3Text.getText());
			angle = Double.parseDouble(angleText.getText());
			
		    }
		catch (NumberFormatException ex)
		    {
			side = oldSide;
			side2 = oldSide2;
			side3 = oldSide3;
			angle = oldAngle;
		    }
	    }
	else if(cancelButton == e.getSource()) 
	    {
		answer = false;
		setVisible(false);
	    }
    }
} 

