// File: Triangle.java
// Author: Dr. Watts
// Contents: This file contains the description and implementation
// of a class called Triangle. 

import java.awt.*;

import static java.lang.Math.*;
import java.awt.geom.*;

public class Triangle extends Shape
{
    protected int [] vertexX = new int [3];
    protected int [] vertexY = new int [3];
    protected Polygon polygon = new Polygon (vertexX, vertexY, 3);
    protected double angle = 0.0;

    Triangle ()
    {
    }


    public void updateVertices()
    {
	vertexX = new int[3];
        vertexY = new int[3];
        PathIterator i = polygon.getPathIterator(new AffineTransform());
	for (int s = 0; s < 3; s++) {

            double[] xy = new double[2];
            i.currentSegment(xy);
            vertexX[s] = (int)xy[0];
            vertexY[s] = (int)xy[1];

            i.next();

	}
    }

    public void resize(double distance)
    {
    	double newdistance = distance;
    	//speed limiting the growth
    	if (newdistance > 5)
    		newdistance = 5;
    	if (newdistance <-5)
    		newdistance = -5;
    	//resizing
    	side += (side * (newdistance/100));
    	calculateTriangle();
    	rotate();
    }

    public void updateSides(double [] xpoints, double [] ypoints)
    {
	side = (int)distance(xpoints[0], xpoints[1], ypoints[0], ypoints[1]);
    }		

    public String getName ()
    {
	return "Triangle";
    }
    public void setCenterX (int X)
    {
	centerX = X;
	calculateTriangle ();
    }
    public void setCenterY (int Y)
    {
	centerY = Y;
	calculateTriangle ();
    }
    public void paintComponent (Graphics2D g2)
    {
    	g2.setStroke(new BasicStroke(strokeSize));
        g2.setPaint(strokeColor);
    	g2.draw (polygon);
        g2.setPaint (color);
	g2.fill (polygon);

	// Draw the center point
	g2.setPaint (Color.BLACK);
	g2.fillOval (centerX-1, centerY-1, 2, 2);  
    }

    protected void calculateTriangle()
    {
	int height = (int)sqrt(pow(side,2)-pow((side/2),2));
	int inCircle = (int)((sqrt(3)/6)*side);
	
	vertexX[0] = centerX-(side/2);
	vertexX[1] = centerX+(side/2);
	vertexX[2] = centerX;
	vertexY[0] = centerY+inCircle;
	vertexY[1] = centerY+inCircle;
	vertexY[2] = centerY-(height-inCircle);
		
		
	polygon.reset();
	for (int i = 0; i<3; i++)
	    polygon.addPoint(vertexX[i], vertexY[i]);
	
    }

    protected void setVertices (int deltaX, int deltaY)
    {
	for (int i=0; i<3; i++)
	    {
		vertexX[i] += deltaX;
		vertexY[i] += deltaY;
	    }
    }
    public void rotate()
    {
	AffineTransform transformer = new AffineTransform();
	transformer.rotate(
			   Math.toRadians(angle), (double)centerX, (double)centerY);
	
	
	Polygon r2 = new Polygon();
        PathIterator i = polygon.getPathIterator(transformer);
        for (int s = 0; s< 3; s++) {
            	    
            double[] xy = new double[2];
            i.currentSegment(xy);
            r2.addPoint((int) xy[0], (int) xy[1]);
	    
	    i.next();
            
        }
	
	polygon = r2;
	
    }

    public void rotate(double Distance)
    {
	angle += (Distance);
	calculateTriangle();
	rotate();
    }

    public boolean isIn (int X, int Y)
    {
	return polygon.contains (X, Y);
    }

    public void move (int deltaX, int deltaY)
    {
	centerX += deltaX;
	centerY += deltaY;
	polygon.translate(deltaX, deltaY);
	updateVertices();//(deltaX, deltaY);	

    }
}
