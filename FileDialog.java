// File Dialog

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FileDialog extends JDialog implements ActionListener 
{
    private JPanel myPanel = null;
    private JButton OKButton = null, cancelButton = null;
    private JPanel buttonPanel= null;
    private JTextField filenameF;
    private String filename;
    private boolean answer = false;
    public boolean getAnswer() { return answer; }
    public String getFilename() { return filename; }
    
 
    public FileDialog(JFrame frame, boolean modal, int x, int y, String title)
    {
	super(frame, modal);
	myPanel = new JPanel();
	getContentPane().add(myPanel);
	myPanel.setLayout (new GridLayout(2,1));
	addTextAndButtons();
	setTitle (title);
	setLocation (x, y);
	setSize (300,150);
	setVisible(true);
    }
    
    private void addTextAndButtons ()
    {
	myPanel.add(new JLabel("Enter the filename"));
	filenameF = new JTextField("", 20);
	filenameF.addActionListener(this);
	myPanel.add (filenameF);
	
	buttonPanel = new JPanel();
	OKButton = new JButton("OK");
	OKButton.addActionListener(this);
	buttonPanel.add(OKButton); 
	cancelButton = new JButton("Cancel");
	cancelButton.addActionListener(this);
	buttonPanel.add(cancelButton); 
	myPanel.add(buttonPanel); 
    }
    
    public void actionPerformed(ActionEvent e) 
    {
	filename = filenameF.getText();
	if (OKButton == e.getSource())
	    {
		answer = true;
		setVisible(false);
	    }
	else if(cancelButton == e.getSource()) 
	    {
		answer = false;
		setVisible(false);
	    }
    }
    
} 

