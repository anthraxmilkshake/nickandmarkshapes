// File: Square.java
// Author: Dr. Watts
// Contents: This file contains the description and implementation
// of a class called Square. 
import java.text.NumberFormat;
import static java.lang.Math.*;
import java.awt.*;
import javax.swing.*;

public final class Square extends Quadrilateral
{

    public Square ()
    {
	
    }
	
    public Square (Square R)
    {
	side = R.side;
	centerX = R.centerX;
	centerY = R.centerY;
	color = R.color;
	angle = R.angle;

	updatePoly();
	rotateQuad();
	
    }
    public void modifyShape (JFrame frame, int x, int y)
    {
	SquareDialog squaredialog = new SquareDialog (frame, true, x, y, side, angle); 
	side = squaredialog.getRadius ();
	color = squaredialog.getColor ();
	angle  =squaredialog.getAngle();

	updatePoly();
	rotateQuad();
    }
    public Square (double S1)
    {
	side = (int)S1;

	updatePoly();
	rotateQuad();

    }
    public Square (int S, int X, int Y, Color C, double R)
    {
	side = S;
	centerX = X;
	centerY = Y;
	color = C;
	angle = R;

	updatePoly();
	rotateQuad();


    }
	
    public void setSide1 (double S1)
    {
	side = (int)S1;

	updatePoly();
	rotateQuad();
    }
	
    public double getSide1 ()
    {
	return side;
    }
       
	
    public double perimeter ()
    {
	return side * 4;
    }
	
    public double area ()
    {
	return side * side;
    }
	
    public String getName ()
    {
	return "Square";
    }




    public void fromString (String str)
    {
	String [] parts = str.split (" ");
	try
	    {
		centerX = Integer.parseInt(parts[0]);
		centerY = Integer.parseInt(parts[1]);
		side = Integer.parseInt(parts[2]);

		color = new Color(Integer.parseInt(parts[3]));
		angle = Double.parseDouble (parts[4]);

		updatePoly();
		rotateQuad();
			
	    }
	catch (NumberFormatException e)
	    {
		System.out.println ("File input error - invalid integer");;
	    }
    }
    public String toString ()
    {
	String string = new String ();
	string += centerX + " ";
	string += centerY + " ";
	string += side + " ";

	string += color.getRGB() + " ";
	string += angle + " ";
	return string;
    }
}	
	
