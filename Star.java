// File: Star.java
// Author: Dr. Watts
// Contents: This file contains the description and implementation
// of a class called Star. 

import java.text.NumberFormat;
import static java.lang.Math.*;
import java.awt.*;
import javax.swing.*;

public final class Star extends cPolygon
{
    

    public Star ()
    {
	sides = 10;
    }
    
    public Star (Star R)
    {
	side = R.side;
	sides = 10;
	centerX = R.centerX;
	centerY = R.centerY;
	color = R.color;
	angle = R.angle;
	
	calculatePoints();
	rotatePoly();
	
    }

    
    public Star (int S, int X, int Y, Color C, double R)
    {
	side = S;
	sides = 10;
	centerX = X;
	centerY = Y;
	color = C;
	angle = R;
	
	calculatePoints();
	rotatePoly();
    }

    public Star (double S1)
    {
	side = (int)S1;
	
	calculatePoints();
	rotatePoly();
    }

    public void modifyShape (JFrame frame, int x, int y)
    {
	StarDialog stardialog = new StarDialog (frame, true, x, y, side, angle); 
	side = stardialog.getRadius ();
	color = stardialog.getColor ();
	angle = stardialog.getAngle();
	
	calculatePoints();
	rotatePoly();
    }

    public void calculatePoints()
    {
	polygon.reset();
        polygon.invalidate();
        double theta = 2 * Math.PI / sides;
        for (int i = 0; i < sides; i++)
            {
                int a = (int)(Math.cos(theta * i) * side);
                int b = (int)(Math.sin(theta * i) * side);
        
                polygon.addPoint(a, b);
		
		i++;
		a = (int)(Math.cos(theta * i) * side * .5);
                b = (int)(Math.sin(theta * i) * side * .5);
        
                polygon.addPoint(a, b);
            }
	polygon.translate(centerX, centerY);
	setVertices();
    }

    public void setSide1 (double S1)
    {
	side = (int)S1;
	
	calculatePoints();
	rotatePoly();
    }
    
    public double getSide1 ()
    {
	return side;
    }
    
    
    public double perimeter ()
    {
	return side * 4;
    }
    
    public double area ()
    {
	return side * side;
    }
    
    public String getName ()
    {
	return "Star";
    }
    
    /*
    public void fromString (String str)
    {
	String [] parts = str.split (" ");
	try
	    {
		centerX = Integer.parseInt(parts[0]);
		centerY = Integer.parseInt(parts[1]);
		side = Integer.parseInt(parts[2]);
		
		color = new Color(Integer.parseInt(parts[3]));
		angle = Double.parseDouble (parts[4]);
		
		calculatePoints();
		rotatePoly();
		
	    }
	catch (NumberFormatException e)
	    {
		System.out.println ("File input error - invalid integer");;
	    }
    }
    public String toString ()
    {
	String string = new String ();
	string += centerX + " ";
	string += centerY + " ";
	string += side + " ";
	
	string += color.getRGB() + " ";
	string += angle + " ";
	return string;
    }
    */
}	
	
