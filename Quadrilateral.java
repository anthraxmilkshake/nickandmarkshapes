// File: Quadrilateral.java
// Author: Dr. Watts
// Contents: This file contains the description and implementation
// of a class called Quadrilateral. 
import java.awt.*;
import java.awt.geom.*;
import static java.lang.Math.*;

public class Quadrilateral extends Shape
{
	
    protected Polygon polyrect = new Polygon();
    protected double angle = 0.0;

    public void resize (double distance)
    {
    	double newdistance = distance;
    	//speed limiting the growth
    	if (newdistance > 5)
    		newdistance = 5;
    	if (newdistance <-5)
    		newdistance = -5;
    	//resizing
    	side += (side * (newdistance/100));
    	updatePoly();
    	rotateQuad();	

    }
    public void rotate(double distance)
    {
	angle += distance;
	updatePoly();
	rotateQuad();	
    }
    
    public void updateSides ()
    {
        int [] vertexX = new int[4];
        int [] vertexY = new int[4];
        PathIterator i = polyrect.getPathIterator(new AffineTransform());
	for (int s = 0; s < 4; s++) {

            double[] xy = new double[2];
            i.currentSegment(xy);
            vertexX[s] = (int)xy[0];
            vertexY[s] = (int)xy[1];

            i.next();

        }
	side = (int)sqrt(((vertexX[2]-vertexX[3])*(vertexX[2]-vertexX[3])) + (((vertexY[2]-vertexY[3])*(vertexY[2]-vertexY[3]))));

    }


    Quadrilateral ()
    {
    }

    public String sides ()
    {
	return "4";
    }
	
    public String getName ()
    {
	return "Quadrilateral";
    }



    public void move (int deltaX, int deltaY)
    {
	centerX += deltaX;
	centerY += deltaY;
	polyrect.translate(deltaX,deltaY);
		
	
    }
    public void updatePoly()
    {
	polyrect.reset();
	polyrect.invalidate();
	polyrect.addPoint(centerX-side/2, centerY-side/2);
	polyrect.addPoint(centerX-side/2, centerY+side/2);
	polyrect.addPoint(centerX+side/2, centerY+side/2);		
	polyrect.addPoint(centerX+side/2, centerY-side/2);
		
    }
    public void rotateQuad()
    {
	AffineTransform transformer = new AffineTransform();
	transformer.rotate(
			   Math.toRadians(angle), (double)centerX, (double)centerY);
	
	
	Polygon r2 = new Polygon();
        PathIterator i = polyrect.getPathIterator(transformer);
        for (int s = 0; s< 4; s++) {
            	    
            double[] xy = new double[2];
            i.currentSegment(xy);
            r2.addPoint((int) xy[0], (int) xy[1]);
	   
	    i.next();
            
        }
	
	polyrect = r2;
	
    }

    public void paintComponent(Graphics2D g2)
    {
	g2.setStroke(new BasicStroke(strokeSize));
    g2.setPaint(strokeColor);
   	g2.draw(polyrect);
	g2.setPaint(color);
	g2.fill(polyrect);

	g2.setPaint(Color.BLACK);	
	g2.fillOval (centerX-1, centerY-1, 2,2);

    }
    public boolean isIn (int X, int Y)
    {

	return  polyrect.contains(X, Y);

    }
}
